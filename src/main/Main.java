/**
 * Pour cette classe nous avons besoins des differentes vues ainsi que des controleurs presents dans les packages vues et controleur
 * Elle necessite egalement la classe et les methode JFrame de swing
 */

package main;

import javax.swing.JFrame;

import controleur.ControleurLampe;
import controleur.ControleurMenu;
import vues.*;

/**
 * La classe Main est la classe principale du jeu Switch off the light, elle permet de tout afficher et de lier les controleurs au différentes vues / modeles
 * @author Charlie Kieffer Theo Chapelle
 */
public class Main {
    /**
     * la methode main, qui s execute lors du lancement de la classe
     * @param args les arguments passes en parametres lors de l appelle de la classe ( pas utilise pour notre projet )
     */
    public static void main(String[] args) {
        /**
         * On creer en premier temps la fenetre qui s affichera pour le joueur
         */
        JFrame fenetre = new JFrame();
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /**
         * On ajoute ensuite toute les informations a la fenetre (affichage, titre, visibilite, ...)
         */
        Affichage aff = new Affichage();
        fenetre.setContentPane(aff);
        fenetre.setTitle("Switch off the light !");
        fenetre.pack();
        fenetre.setVisible(true);
        /**
         * on ajoute enfin les controleur a l affichage 
         * ControleurLampe et ControleurMenu
         */
        ControleurLampe cl = new ControleurLampe(aff.getGrille(), aff.getMenu());
        ControleurMenu cm = new ControleurMenu(aff.getGrille(), aff.getMenu());
    }
}