package modele;

import java.io.Serializable;

import vues.Grille;

/**
 * Classe modélisant la propagation en mode Jeu
 */
public class PropagationJeu implements Propagation, Serializable {
    
    /**
     * Lien vers le plateau actuel
     */
    private Plateau plateau;

    /**
     * Constructeur qui construit la propagation avec le plateau donné
     * @param p plateau donné
     */
    public PropagationJeu(Plateau p) {
        this.plateau = p;
    }

    /**
     * Méthode de propagation en mode Jeu
     * @param x coordonnée x de la lumière
     * @param y coordonnée y de la lumière
     */
    @Override
	public void propager(int x, int y) {
        this.plateau.selectionner(x, y);
        if (x > 0) {
            this.plateau.selectionner(x - 1, y);
        }
        if (y > 0) {
            this.plateau.selectionner(x, y - 1);
        }
        if (x < Plateau.NB_LUMIERES_PAR_LIGNE - 1) {
            this.plateau.selectionner(x + 1, y);
        }
        if (y < Plateau.NB_LUMIERES_PAR_COLONNE - 1) {
            this.plateau.selectionner(x, y + 1);
        }
	}

}