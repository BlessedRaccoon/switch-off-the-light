package vues;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import modele.*;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Classe représentant la Grille de jeu
 */
public class Grille extends JPanel implements Observer {

    /**
     * Plateau du jeu
     */
    private Plateau plateau;

    /**
     * Constant de la taille de la lampe
     */
    public static final int TAILLE_LAMPE = 50;

    /**
     * JPanel représentant l'affichage de chaque lampe
     */
    private JPanel[][] lampes;

    /**
     * Couleur d'une lampe allumée
     */
    private static final Color COULEUR_ALLUME = new Color(0, 255, 0);

    /**
     * Couleur d'une lampe éteinte
     */
    private static final Color COULEUR_ETEINT = new Color(0, 127, 0);

    /**
     * Constructeur qui initialise la vue
     */
    public Grille() {
        super();
        this.setLayout(new GridLayout(Plateau.NB_LUMIERES_PAR_COLONNE, Plateau.NB_LUMIERES_PAR_LIGNE));
        this.plateau = new Plateau();
        this.lampes = new JPanel[Plateau.NB_LUMIERES_PAR_COLONNE][Plateau.NB_LUMIERES_PAR_LIGNE];
        for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                this.lampes[i][j] = new JPanel();
                this.lampes[i][j].setPreferredSize(new Dimension(TAILLE_LAMPE, TAILLE_LAMPE));
                this.lampes[i][j].setBackground(COULEUR_ETEINT);
                this.lampes[i][j].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                this.add(lampes[i][j]);
                this.plateau.getLumieres()[i][j].addObserver(this);
            }
        }
        this.setBackground(Color.BLACK);
        this.setPreferredSize(new Dimension(Plateau.NB_LUMIERES_PAR_LIGNE * TAILLE_LAMPE, Plateau.NB_LUMIERES_PAR_COLONNE * TAILLE_LAMPE));
    }

    /**
     * Méthode paintComponent
     * @param g graphics
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                boolean flag = this.plateau.getLumieres()[i][j].getAllume();
                this.lampes[i][j].setBackground((flag) ? COULEUR_ALLUME : COULEUR_ETEINT);
            }
        }
    }

    /**
     * Méthode update qui se contente de rafraîchir l'affichage
     * @param arg0 observable
     * @param arg1 object
     */
    @Override
    public void update(Observable arg0, Object arg1) {
        repaint();
    }

    /**
     * Getter du plateau
     * @return Plateau
     */
    public Plateau getPlateau() {
        return this.plateau;
    }

    /**
     * Getter des lampes
     * @return les lampes
     */
    public JPanel[][] getLampes() {
        return this.lampes;
    }

    /**
     * Setter du plateau
     * @param p plateau
     */
    public void setPlateau(Plateau p) {
        this.plateau = p;
    }

}