package vues;

import javax.swing.JPanel;
import java.awt.*;

/**
 * JPanel contenant toute l'interface graphique
 */
public class Affichage extends JPanel {

    /**
     * Grille de jeu
     */
    private Grille grille;

    /**
     * Menu du jeu
     */
    private Menu menu;

    /**
     * Constructeur standard qui crée un affichage
     */
    public Affichage() {
        this.setLayout(new BorderLayout());
        this.grille = new Grille();
        this.menu = new Menu();
        this.add(this.grille, BorderLayout.EAST);
        this.add(this.menu, BorderLayout.WEST);
    }

    /**
     * Getter du menu
     * @return menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Setter du menu
     * @param menu menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * Getter de la grille
     * @return grille
     */
    public Grille getGrille() {
        return grille;
    }

    /**
     * Setter de la grille
     * @param grille grille
     */
    public void setGrille(Grille grille) {
        this.grille = grille;
    }

}