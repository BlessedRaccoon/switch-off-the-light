package vues;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.GridLayout;

/**
 * Classe contenant le paneau de contrôle du jeu
 */
public class Menu extends JPanel {

    /**
     * Bouton "Configurer"
     */
    private JButton configurer;

    /**
     * Bouton "Sauvegarder"
     */
    private JButton sauvegarder;

    /**
     * Bouton "Recharger"
     */
    private JButton recharger;

    /**
     * Bouton "Aleatoire"
     */
    private JButton aleatoire;

    /**
     * Bouton "Jouer"
     */
    private JButton jouer;

    /**
     * Bouton "Quitter"
     */
    private JButton quitter;

    /**
     * Label "Nombre de déplacements"
     */
    private JLabel nbDep;

    /**
     * Label de la valeur du nombre de déplacements
     */
    private JLabel valeurDep;

    /**
     * Constructeur standard du menu
     */
    public Menu() {
        this.setLayout(new GridLayout(8, 1));
        this.configurer = new JButton("Configurer");
        this.aleatoire = new JButton("Aléatoire");
        this.jouer = new JButton("Jouer");
        this.nbDep = new JLabel("Nombre de déplacements");
        this.valeurDep = new JLabel("0", SwingConstants.CENTER);
        this.sauvegarder = new JButton("Sauvegarder");
        this.recharger = new JButton("Recharger");
        this.quitter = new JButton("Quitter");
        this.add(configurer);
        this.add(aleatoire);
        this.add(jouer);
        this.add(nbDep);
        this.add(valeurDep);
        this.add(sauvegarder);
        this.add(recharger);
        this.add(quitter); 
    }

    /**
     * Getter de la valeur du nombre de déplacements
     * @return nombre de déplacements
     */
    public JLabel getValeurDep() {
        return valeurDep;
    }

    /**
     * Setter du nombre de déplacements
     * @param valeurDep nouveau nombre de déplacements
     */
    public void setValeurDep(JLabel valeurDep) {
        this.valeurDep = valeurDep;
    }

    /**
     * Getter du bouton "Quitter"
     * @return bouton "Quitter"
     */
    public JButton getQuitter() {
        return quitter;
    }

    /**
     * Getter du bouton "Jouer"
     * @return bouton "Jouer"
     */
    public JButton getJouer() {
        return jouer;
    }

    /**
     * Getter du bouton "Aléatoire"
     * @return bouton "Aléatoire"
     */
    public JButton getAleatoire() {
        return aleatoire;
    }

    /**
     * Getter du bouton "Sauvegarder"
     * @return bouton "Sauvegarder"
     */
    public JButton getSauvegarder() {
        return this.sauvegarder;
    }

    /**
     * Getter du bouton "Recharger"
     * @return bouton "Recharger"
     */
    public JButton getRecharger() {
        return this.recharger;
    }

    /**
     * Getter du bouton "Configurer"
     * @return bouton "Configurer"
     */
    public JButton getConfigurer() {
        return configurer;
    }

    /**
     * Méthode qui incrémente le nombre de déplacements
     */
    public void incrementerDeplacement() {
        int valeur = Integer.parseInt(this.valeurDep.getText());
        this.valeurDep.setText("" + (valeur + 1));
    }

}