/**
 * Pour cette classe nous avons besoin de certaines classes des packages vues et modeles
 * ainsi que les classes et methodes de swing et awt pour Event et Font
 */

package controleur;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.*;

import modele.Plateau;
import modele.PropagationJeu;

import java.awt.Font;
import java.awt.event.*;

import vues.Grille;
import vues.Menu;

/**
 * Cette classe ControleurLampe a pour but de faire le controle lors de l appuie des Lampe sur le JPanel de la grille
 * Elle herite notamment de MouseInputAdapter, cela est utile pour trouver sur quel Lampe on clique et gérer les différentes actions a la souris sur la grille
 * @author Charlie Kieffer Theo Chapelle
 */
public class ControleurLampe extends MouseInputAdapter {

    /**
     * Une Grille grille, un JPanel contenant toutes les lumieres
     */
    private Grille grille;
    /**
     * Un Menu menu, un JPanel contenant tout les boutons et le nombre de deplacements realises lors du jeu 
     */
    private Menu menu;

    /**
     * Le constructeur de ControleurLampe
     * @param g la grille utilise 
     * @param m le menu utilise 
     */

    public ControleurLampe(Grille g, Menu m) {
    
        /**
         * on ajoute tout dabord un Listener a toutes les lampes de la grille 
         * pour cela on parcours toute la grille qui est un tableau en 2D
         */
        this.grille = g;
        this.menu = m;
        for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                grille.getLampes()[i][j].addMouseListener(this);
            }
        }
    }

    /**
     * la metode mouseClicked permet de recuperer la position de la lampe dans la grille
     * pour ensuite propager, c est a dire allumer / eteindre la lampe et celles adjacentes 
     * On incremente le nombre de deplacements et on verifie egalement si le joueur a gagne ou non
     * @param e L event lors du clique de la souris 
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        JPanel panel = (JPanel) e.getComponent();
        int xpanel = 0;
        int ypanel = 0;
        JPanel[][] lumieres = grille.getLampes();
        for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                if (panel == lumieres[i][j]) {
                    xpanel = i;
                    ypanel = j;
                    break;
                }
            }
        }
        grille.getPlateau().propager(xpanel, ypanel);
        if (grille.getPlateau().getPropagation() instanceof PropagationJeu) {
            grille.getPlateau().incrementerDeplacement();
            menu.getValeurDep().setText("" + grille.getPlateau().getDeplacements());
        }
        this.gagner();
    }

    /**
     * Cette methode permet d afficher une nouvelle fenetre de victoire si le joueur a reussi a eteindre toutes les lumieres 
     * On affiche egalement le nombre de deplacements du joueur
     */

    private void gagner() {
        if(grille.getPlateau().gagner()) {
            JFrame gagner = new JFrame();
            gagner.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JLabel label = new JLabel("VOUS AVEZ GAGNÉ !! En " + menu.getValeurDep().getText() + " déplacements !");
            label.setFont(new Font("Arial", Font.BOLD, 48));
            JPanel content = new JPanel();
            content.add(label);
            gagner.setContentPane(content);
            gagner.pack();
            gagner.setVisible(true);
        }
    }

}